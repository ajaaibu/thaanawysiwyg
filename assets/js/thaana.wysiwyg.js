/*!
 * jQuery Thaana WYSIWYG v1.0
 * http://twitter.com/ajaaibu
 * http://facebook.com/ajaaibu
 *
 * Copyright 2013, Ahmed Ali (ajaaibu)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * Requires jquery 1.10.2 or later
 *
 * Date: Sun Aug 08 11:45:56 2013 +0530
 */
(function($){

	$.fn.thaanaeditor = function(options){
		var settings = {
            keyboard: 'phonetic',
            buttons : Array('formatting','fontname','fontsize','forecolor','backcolor','sep','bold','underline','unordered','ordered','quote','left_align','center_align','right_align','removeFormat','undo','redo','hyperlink','unlink'),
            width:620,
            height: 200,
        };
        
        var keyboards = { 
    		'phonetic' : { 33 : '!', 34 : '"', 35: '#', 36 : '$', 37 : '%', 38 : '&', 39 : '\'', 40 : ')', 41 : '(', 42 : '*', 43 : '+', 44 : '،', 45 : '-', 46 : '.', 47 : '/', 58: ':', 59 : '؛', 60 : '>', 61 : '=', 62 : '<', 63 : '؟', 64 : '@', 65 : 'ާ', 66 : 'ޞ', 67 : 'ޝ', 68 : 'ޑ', 69 : 'ޭ', 70 : 'ﷲ', 71 : 'ޣ', 72 : 'ޙ', 73 : 'ީ' ,74 : 'ޛ', 75 : 'ޚ' ,76 : 'ޅ', 77 : 'ޟ', 78 : 'ޏ', 79 : 'ޯ', 80 : '÷', 81 : 'ޤ', 82 : 'ޜ', 83 : 'ށ', 84 : 'ޓ', 85 : 'ޫ', 86 : 'ޥ', 87 : 'ޢ', 88 : 'ޘ', 89 : 'ޠ', 90 : 'ޡ', 91 : ']', 92 : '\\', 93 : '[', 94 : '^', 95: '_', 96 : '`', 97 : 'ަ', 98 : 'ބ', 99 : 'ޗ', 100 : 'ދ', 101 : 'ެ', 102 : 'ފ', 103 : 'ގ', 104 : 'ހ', 105 : 'ި', 106 : 'ޖ', 107 : 'ކ', 108 : 'ލ', 109 : 'މ', 110 : 'ނ', 111 : 'ޮ', 112 : 'ޕ', 113 : 'ް', 114 : 'ރ', 115 : 'ސ', 116 : 'ތ', 117 : 'ު', 118 : 'ވ', 119 : 'އ', 120 : '×', 121 : 'ޔ', 122 : 'ޒ', 123: '}', 124 : '|', 125 : '{', 126 : '~'},
    		'typewriter' : { 33 : '!', 34 : '؛', 35: '#', 36 : '$', 37 : '%', 38 : '&', 39 : 'ﷲ', 40 : ')', 41 : '(', 42 : '*', 43 : '+', 44 : 'ށ', 45 : '-', 46 : 'ޓ', 47 : 'ޯ', 58: 'ޡ', 59 : 'ފ', 60 : '\\', 61 : '=', 62 : 'ޞ', 63 : '؟', 64 : '@', 65 : '<', 66 : 'ޟ', 67 : 'ޏ', 68 : '.', 69 : '“', 70 : '،', 71 : '"', 72 : 'ޥ', 73 : 'ޣ' ,74 : 'ޢ', 75 : 'ޘ' ,76 : 'ޚ', 77 : 'ޝ', 78 : 'ޛ', 79 : 'ޠ', 80 : 'ޙ', 81 : '×', 82 : '/', 83 : '>', 84 : ':', 85 : 'ޜ', 86 : 'ޗ', 87 : '’', 88 : 'ޕ', 89 : 'ޤ', 90 : 'ޖ', 91 : 'ލ', 92 : ']', 93 : '[', 94 : '^', 95: '_', 96 : '`', 97 : 'ި', 98 : 'ޅ', 99 : 'ސ', 100 : 'ް', 101 : 'ާ', 102 : 'ަ', 103 : 'ެ', 104 : 'ވ', 105 : 'މ', 106 : 'އ', 107 : 'ނ', 108 : 'ކ', 109 : 'ބ', 110 : 'ދ', 111 : 'ތ', 112 : 'ހ', 113 : 'ޫ', 114 : 'ީ', 115 : 'ު', 116 : 'ޭ', 117 : 'ރ', 118 : 'ޔ', 119 : 'ޮ', 120 : 'ޑ', 121 : 'ގ', 122 : 'ޒ', 123: '÷', 124 : '}', 125 : '{', 126 : '~'}
    	};

    	objectToHTML = function(button){

	        switch(button.type){
	        	case 'select':
	        		return generateSelect(button);
	        	break;
	        	case 'image':
	        		return generateImageButton(button);
	        	break;
	        	case 'seperator':
	        		return '<br />';
	        }
        };

        generateImageButton = function(button){
        	var element = $('<input type="image" value=""/>')
        	.attr({'id':button.name, 'src': button.img})
        	.on('click',function(){value = button.value ? button.value : false;
        		if(button.prompt){
        			link = window.prompt(button.prompt,button.promptfill);

        			if(link){
        				value = link;
        				formatDoc(button.frame,button.command,value);
        			}
        		}
        		else{
        			value = button.value ? button.value : false;
        			formatDoc(button.frame,button.command,value);
        		}
        		
        		return false;
        	});

        	return element;
        };

        generateSelect = function(button){
        	var element = $('<select></select>')
        	.attr('id',button.name)
        	.append('<option selected>'+button.name+'</option>')
        	.on('change',function(){
        		formatDoc(button.frame,button.command,eval(button.value));
        		if(button.callback){
        			eval(button.callback);
        		}
        	});

        	for(value in button.options){
        		var option = $('<option value="'+value+'">'+button.options[value]+'</option>');
        		if(button.styleWithValue){
        			$(option).css(button.styleName,value);
        		}
        		element.append(option);
        	}
        	return element;
        };

    	
    	return this.each(function(){

    		var textArea = {
	            id: $(this).attr('id'),
	            wysiwyg: $(this).attr('id')+'_editor',
	            frame: $(this).attr('id')+'_frame'
	        };

	        var buttons = {
	        	formatting : {
	        		frame: textArea['frame'],
	        		name: 'Formatting',
	        		type: 'select',
	        		command: 'formatblock',
	        		value: '$(this).find("option:selected").val()',
	        		callback: '$(this).children(":selected").removeAttr("selected")',
	        		options: {
	        			h1: 'Title 1 &lt;h1&gt;',
	        			h2: 'Title 2 &lt;h2&gt;',
	        			h3: 'Title 3 &lt;h3&gt;',
	        			h4: 'Title 4 &lt;h4&gt;',
	        			h5: 'Title 5 &lt;h5&gt;',
	        			h6: 'Subtitle &lt;h6&gt;',
	        			p: 'Paragraph &lt;p&gt;',
	        			pre: 'Preformatted &lt;pre&gt;'
	        		},
	        	},
	        	fontname: {
	        		frame: textArea['frame'],
	        		name: 'Font',
	        		type: 'select',
	        		command: 'fontname',
	        		value: '$(this).find("option:selected").val()',
	        		callback: '$(this).children(":selected").removeAttr("selected")',
	        		options : {
	        			mvfaseyha: 'MV Faseyha',
	        			mvwaheed: 'MV Waheed',
	        		}
	        	},
	        	fontsize: {
	        		frame: textArea['frame'],
	        		name: 'Size',
	        		type: 'select',
	        		command: 'fontsize',
	        		value: '$(this).find("option:selected").val()',
	        		callback: '$(this).children(":selected").removeAttr("selected")',
	        		options: {
	        			1 : 'Very Small',
	        			2 : 'A bit small',
	        			3 : 'Normal',
	        			4 : 'Medium-large',
	        			5 : "Big",
	        			6 : 'Very Big',
	        			7 : 'Maximum'
	        		}
	        	},
	        	forecolor: {
	        		frame: textArea['frame'],
	        		name: 'Color',
	        		type: 'select',
	        		command: 'forecolor',
	        		value: '$(this).children(":selected").val()',
	        		callback: '$(this).children(":selected").removeAttr("selected")',
	        		styleWithValue : true,
	        		styleName: 'color',
	        		options: {
	        			red: 'Red', blue: 'Blue', green: 'Green', black: 'Black', white: 'White'
	        		}
	        	},
	        	sep: {
	        		type: 'seperator',
	        	},
	        	backcolor: {
	        		frame: textArea['frame'],
	        		name: 'BackColor',
	        		type: 'select',
	        		command: 'backcolor',
	        		value: '$(this).children(":selected").val()',
	        		callback: '$(this).children(":selected").removeAttr("selected")',
	        		styleWithValue : true,
	        		styleName: 'background-color',
	        		options: {
	        			red: 'Red', blue: 'Blue', green: 'Green', black: 'Black', white: 'White'
	        		}
	        	},
	        	bold: {
	        		frame: textArea['frame'],
	        		name: 'Bold',
	        		type: 'image',
	        		command: 'bold',
	        		img: 'data:image/gif;base64,R0lGODlhFgAWAID/AMDAwAAAACH5BAEAAAAALAAAAAAWABYAQAInhI+pa+H9mJy0LhdgtrxzDG5WGFVk6aXqyk6Y9kXvKKNuLbb6zgMFADs=',
	        	},
	        	italic: {
	        		frame: textArea['frame'],
	        		name: 'Italic',
	        		type: 'image',
	        		command: 'italic',
	        		img: 'data:image/gif;base64,R0lGODlhFgAWAKEDAAAAAF9vj5WIbf///yH5BAEAAAMALAAAAAAWABYAAAIjnI+py+0Po5x0gXvruEKHrF2BB1YiCWgbMFIYpsbyTNd2UwAAOw==',
	        	},
	        	underline: {
	        		frame: textArea['frame'],
	        		name: 'Underline',
	        		type: 'image',
	        		command: 'underline',
	        		img: 'data:image/gif;base64,R0lGODlhFgAWAKECAAAAAF9vj////////yH5BAEAAAIALAAAAAAWABYAAAIrlI+py+0Po5zUgAsEzvEeL4Ea15EiJJ5PSqJmuwKBEKgxVuXWtun+DwxCCgA7',
	        	},
	        	undo: {
	        		frame: textArea['frame'],
	        		name: 'Undo',
	        		type: 'image',
	        		command: 'undo',
	        		img: 'data:image/gif;base64,R0lGODlhFgAWAOMKADljwliE33mOrpGjuYKl8aezxqPD+7/I19DV3NHa7P///////////////////////yH5BAEKAA8ALAAAAAAWABYAAARR8MlJq7046807TkaYeJJBnES4EeUJvIGapWYAC0CsocQ7SDlWJkAkCA6ToMYWIARGQF3mRQVIEjkkSVLIbSfEwhdRIH4fh/DZMICe3/C4nBQBADs=',
	        	},
	        	redo: {
	        		frame: textArea['frame'],
	        		name: 'Undo',
	        		type: 'image',
	        		command: 'redo',
	        		img: 'data:image/gif;base64,R0lGODlhFgAWAMIHAB1ChDljwl9vj1iE34Kl8aPD+7/I1////yH5BAEKAAcALAAAAAAWABYAAANKeLrc/jDKSesyphi7SiEgsVXZEATDICqBVJjpqWZt9NaEDNbQK1wCQsxlYnxMAImhyDoFAElJasRRvAZVRqqQXUy7Cgx4TC6bswkAOw==',
	        	},
	        	removeFormat: {
	        		frame: textArea['frame'],
	        		name: 'Remove Format',
	        		type: 'image',
	        		command: 'removeFormat',
	        		img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAYAAADEtGw7AAAABGdBTUEAALGPC/xhBQAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAAd0SU1FB9oECQMCKPI8CIIAAAAIdEVYdENvbW1lbnQA9syWvwAAAuhJREFUOMtjYBgFxAB501ZWBvVaL2nHnlmk6mXCJbF69zU+Hz/9fB5O1lx+bg45qhl8/fYr5it3XrP/YWTUvvvk3VeqGXz70TvbJy8+Wv39+2/Hz19/mGwjZzuTYjALuoBv9jImaXHeyD3H7kU8fPj2ICML8z92dlbtMzdeiG3fco7J08foH1kurkm3E9iw54YvKwuTuom+LPt/BgbWf3//sf37/1/c02cCG1lB8f//f95DZx74MTMzshhoSm6szrQ/a6Ir/Z2RkfEjBxuLYFpDiDi6Af///2ckaHBp7+7wmavP5n76+P2ClrLIYl8H9W36auJCbCxM4szMTJac7Kza////R3H1w2cfWAgafPbqs5g7D95++/P1B4+ECK8tAwMDw/1H7159+/7r7ZcvPz4fOHbzEwMDwx8GBgaGnNatfHZx8zqrJ+4VJBh5CQEGOySEua/v3n7hXmqI8WUGBgYGL3vVG7fuPK3i5GD9/fja7ZsMDAzMG/Ze52mZeSj4yu1XEq/ff7W5dvfVAS1lsXc4Db7z8C3r8p7Qjf///2dnZGxlqJuyr3rPqQd/Hhyu7oSpYWScylDQsd3kzvnH738wMDzj5GBN1VIWW4c3KDon7VOvm7S3paB9u5qsU5/x5KUnlY+eexQbkLNsErK61+++VnAJcfkyMTIwffj0QwZbJDKjcETs1Y8evyd48toz8y/ffzv//vPP4veffxpX77z6l5JewHPu8MqTDAwMDLzyrjb/mZm0JcT5Lj+89+Ybm6zz95oMh7s4XbygN3Sluq4Mj5K8iKMgP4f0////fv77//8nLy+7MCcXmyYDAwODS9jM9tcvPypd35pne3ljdjvj26+H2dhYpuENikgfvQeXNmSl3tqepxXsqhXPyc666s+fv1fMdKR3TK72zpix8nTc7bdfhfkEeVbC9KhbK/9iYWHiErbu6MWbY/7//8/4//9/pgOnH6jGVazvFDRtq2VgiBIZrUTIBgCk+ivHvuEKwAAAAABJRU5ErkJggg==',
	        	},
	        	left_align: {
	        		frame: textArea['frame'],
	        		name: 'Left Align',
	        		type: 'image',
	        		command: 'justifyleft',
	        		img: 'data:image/gif;base64,R0lGODlhFgAWAID/AMDAwAAAACH5BAEAAAAALAAAAAAWABYAQAIghI+py+0Po5y02ouz3jL4D4JMGELkGYxo+qzl4nKyXAAAOw==',
	        	},
	        	center_align: {
	        		frame: textArea['frame'],
	        		name: 'Center Align',
	        		type: 'image',
	        		command: 'justifycenter',
	        		img: 'data:image/gif;base64,R0lGODlhFgAWAID/AMDAwAAAACH5BAEAAAAALAAAAAAWABYAQAIfhI+py+0Po5y02ouz3jL4D4JOGI7kaZ5Bqn4sycVbAQA7',
	        	},
	        	right_align: {
	        		frame: textArea['frame'],
	        		name: 'Right Align',
	        		type: 'image',
	        		command: 'justifyright',
	        		img: 'data:image/gif;base64,R0lGODlhFgAWAID/AMDAwAAAACH5BAEAAAAALAAAAAAWABYAQAIghI+py+0Po5y02ouz3jL4D4JQGDLkGYxouqzl43JyVgAAOw==',
	        	},
	        	ordered: {
	        		frame: textArea['frame'],
	        		name: 'Ordered',
	        		type: 'image',
	        		command: 'insertorderedlist',
	        		img: 'data:image/gif;base64,R0lGODlhFgAWAMIGAAAAADljwliE35GjuaezxtHa7P///////yH5BAEAAAcALAAAAAAWABYAAAM2eLrc/jDKSespwjoRFvggCBUBoTFBeq6QIAysQnRHaEOzyaZ07Lu9lUBnC0UGQU1K52s6n5oEADs=',
	        	},
	        	unordered: {
	        		frame: textArea['frame'],
	        		name: 'Un Ordered',
	        		type: 'image',
	        		command: 'insertunorderedlist',
	        		img: 'data:image/gif;base64,R0lGODlhFgAWAMIGAAAAAB1ChF9vj1iE33mOrqezxv///////yH5BAEAAAcALAAAAAAWABYAAAMyeLrc/jDKSesppNhGRlBAKIZRERBbqm6YtnbfMY7lud64UwiuKnigGQliQuWOyKQykgAAOw==',
	        	},
	        	quote: {
	        		frame: textArea['frame'],
	        		name: 'quote',
	        		type: 'image',
	        		command: 'formatblock',
	        		value: 'blockquote',
	        		img: 'data:image/gif;base64,R0lGODlhFgAWAIQXAC1NqjFRjkBgmT9nqUJnsk9xrFJ7u2R9qmKBt1iGzHmOrm6Sz4OXw3Odz4Cl2ZSnw6KxyqO306K63bG70bTB0rDI3bvI4P///////////////////////////////////yH5BAEKAB8ALAAAAAAWABYAAAVP4CeOZGmeaKqubEs2CekkErvEI1zZuOgYFlakECEZFi0GgTGKEBATFmJAVXweVOoKEQgABB9IQDCmrLpjETrQQlhHjINrTq/b7/i8fp8PAQA7',
	        	},
	        	hyperlink: {
	        		frame: textArea['frame'],
	        		name: 'Hyperlink',
	        		type: 'image',
	        		command: 'createLink',
	        		prompt : 'Write the URL here',
	        		promptfill: 'http://',
	        		img: 'data:image/gif;base64,R0lGODlhFgAWAOMKAB1ChDRLY19vj3mOrpGjuaezxrCztb/I19Ha7Pv8/f///////////////////////yH5BAEKAA8ALAAAAAAWABYAAARY8MlJq7046827/2BYIQVhHg9pEgVGIklyDEUBy/RlE4FQF4dCj2AQXAiJQDCWQCAEBwIioEMQBgSAFhDAGghGi9XgHAhMNoSZgJkJei33UESv2+/4vD4TAQA7',
	        	},
	        	unlink: {
	        		frame: textArea['frame'],
	        		name: 'Unlink',
	        		type: 'image',
	        		command: 'unlink',
	        		img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAMAAADzapwJAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAwBQTFRFtMHRlMdlq6ur+O6nOm/V/fsG8LQRxtn2L1qt////lKvQo8TtV2yKRkVGq9GM/2hombDR/zExL0+PuNj5p73uUlJS5Ov209PU3d3dj6CyydPmurq6hKPfxMrViafWyeL8kaTC6enpX08/e3t8U4fgdaTjPXAgkLH2kZKRi7tlwtu+r8rwc4GVoLDAMTIxiJey7fT6rMTxwcLE8PD/M0Zi7MhXhISEy8vMIDWDcprgioZU2vv/sKaOwc7jEBgQAAAAcnJyk62EtLO0KiUq1d3sZ5RIaZfcd6hMUHDAm8HyirH07PH3ZpTwi4uLnZ2cRXvgYWFh/wMD0JEAVI0qh7LnWX3QYnin0qRdzsiP8OKMLkBnRGOgTHsxgG9UUIbwaIrm8Pj+5diF9fb3bW1tSoXFSFyIe6TV8MIz/4uLvczs0Ob91+P1iGcnX4zS0N/x8PDwT4zHm7jwWny8o4AxsLnOZnaJ+fn68NJsb5bwdZfF5+71gaPHobPZG0KTnqfBsbHBZJfJz9fvkrXtUFtrZ4fytMPuz/X/4+PjjbTcT0pPP2exSnbFqJRXfJq/3+//o6mz/7W1YYClfJzcb4CgP1+w4/f/2uHunrrl3OX4n7DQscDfUIDgPVmgao3SnbXjYFpgv8nggK5tjoVv8fT4/1BQqbvekJWgoJWAf63lj6zf1tfZYYfIIU+h/Pz97f//b1xM8PP3mLzqqLjVgLamn7vaYoe5T3y4Tz8vh7W3Qk5bUHjQ4OjxkcCHure6wNHyapqhYJKNlrXjwL2/gr5VVI/KHx8gEBARIBggPy8fDgkP//d7AP//AAD/YJDf/wD/T4DQXpDwcIq/mpiacaHv16AJ//j/gqbvkJiwAIAA0NDRwLCAwMDAkIiAb2dK4eb1gIiggJCsV1tXoJhvtNHy0Oa6o6OjSlJKz9HOzs3NeKqff7OhYoDrXmx9qbC4lpeWgYGC4v///8jI1+j+kKjwWYNCcoFg6fD+HycfLCwuPT0+lrnhPmnAcJ7pfqL/Zmdn////7JSISQAAAQB0Uk5T////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////AFP3ByUAAADLSURBVHjaYvzPgA0wMdBe+EnozE3iK0Nhwowwl+x/de5eSDiqsIwR4/9FAgX3GP+fewIR//////8yDUWJ2xr//SQkjDTK/v////8/CwMDA0OqyWIb5Y0Mos+6j3xNRRii+d5H2JwhaB3DrZ3XBa/DDWG6+Z6XV/S/BC/v+5tM//////+f4f////8lNJyY3jn8V37H5KQh8f//////Gf8zMDCsWyx8V7lCRUWWQfltbBCKu60in1ywr0L35WRetmUaHqIYvqR/eKMBwADB3VK7X4olFwAAAABJRU5ErkJggg==',
	        	}
	        };

    		// extend settings with options if anything is defined
    		if( options ){
	            $.extend(settings,options);
	        }

	        var editorContainer = $('<div class="thaanaWysiwyg" id="'+textArea['frame']+'" style="width:'+(settings.width-2)+'px;height:'+(settings.height-2)+'px;"><div class="editor-controls"></div></div>'),
	       		contentFrame = $('<iframe frameborder="0" width="100%" src="javascript:true;"></iframe>');
	       	editorContainer.append(contentFrame);
	       	// apend the iframe and hide the normal textarea
	       	$(this).after(editorContainer).hide();

	       	var frame = contentFrame[0].contentWindow.document;
	       	frame.open();
	       	frame.write(
	       		'<!DOCTYPE html>'
	       		+ '<html>'
	       		+ '<head>'
	       		+ '<meta charset="utf-8"/>'
	       		+ '<style>'
	       		+ '\
	       		@font-face {  font-family: mvfaseyha;  src: url( ./assets/fonts/mv_faseyha.otf ) format("opentype");  }\
	       		body{ font-family: mvfaseyha; direction:rtl;background:#fff;}\
	       		'
	       		+ '</style>'
	       		+ '</head>'
	       		+ '<body contenteditable="true" class="editor-document">'
	       		+ '</body>'
	       		+ '</html>'
	       	);
	       	frame.close();

	       	var controls = $('#'+textArea['frame']).find('.editor-controls'),
	       		totalButtons = settings.buttons.length;

	       	// append the defined buttons to the editor-controls
	       	for(b in settings.buttons){
	       		var btn = settings.buttons[b];
	       		if(buttons[btn]){
	       			var button = objectToHTML(buttons[btn]);
	       			controls.append(button);
	       		}

	       		if(b == totalButtons-1){
	       			var controlsHeight = $('#'+textArea['frame']).find('.editor-controls').outerHeight(),
	       				adjustable = settings.height-2-controlsHeight-11	;
	       			$('#'+textArea['frame']).find('iframe').height(adjustable);
	       		}
	       	}

	       	var editor = editorContainer.find('iframe').contents().find('body');

	       	// adjust editor height and set value
	        editor.html($('#'+textArea['id']).val());
	        // set editor to design mode
	        editor.designMode = 'On';

	        // handle keyboard events
	        $(editor).keypress(function(e){
				if(e.ctrlKey){
	            	return true;
	           	}
           		else if(e.which == 16){
	            	return true;
	          	 }
	          	 else if(keyboards[settings.keyboard][e.which]){
	          	 	e.preventDefault();
	          	 	if(document.selection && document.selection.createRange){ // IE
			        var range = document.selection.createRange();
			            if(range.pasteHTML){
			                range.pasteHTML(keyboards[settings.keyboard][e.which]);
			            }
			        }
	          	 	else{
	        			frame.execCommand('insertHTML',false,keyboards[settings.keyboard][e.which]);
	        		}
	            	return true;
	           	}
	           	else{
	            	return true;
	           	}

	        }).on('keyup focus',function(){
	        	var html = $(editor).html();
	        	$('#'+textArea['id']).val(html);
	        });

	        formatDoc = function(id,sCmd,sValue){ 
	        	var frame = $('#'+id).find('iframe')[0].contentWindow.document;
				frame.execCommand(sCmd,false,sValue); 
			};
    	});
	};

})(jQuery);